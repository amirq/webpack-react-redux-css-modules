export default {
  home: {
		path: '/',
		title: 'خانه',
	},
  search: {
		path: '/search',
		title: 'جستجو ویدئو',
	}
}