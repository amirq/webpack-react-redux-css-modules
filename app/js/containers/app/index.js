import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';

import Layout from 'js/containers/Layout';

import 'app/scss/base/base';
//import 'js/components/sprite';

export default ({ store, persistor }) => (
	<Provider store={store}>
		<PersistGate persistor={persistor}>
			<Router basename={process.env.PATH_ROOT}>
				<Layout />
			</Router>
		</PersistGate>
	</Provider>
)

