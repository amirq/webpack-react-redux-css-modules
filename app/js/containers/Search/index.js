import React, { Component } from 'react';

import SearchBar from './search-bar';
import VideoDetails from './video-details';
import SearchItems from './search-items';

class Search extends Component {
	render() {
		return (
			<div className="section section--intro">
				<div className="section-wrap">
					<div className="row">
						<div className="col-xs-24 col-md-12">
							<SearchBar/>
							<VideoDetails/>
						</div>
						<aside className="col-xs-24 col-md-12">
							<SearchItems/>
						</aside>
					</div>
				</div>
			</div>
		);
	}
}

export default Search;