import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchData } from '../actions';

import image from 'assets/images/react-logo.svg';
import Icon from 'js/components/icon';

import styles from './styles';

class SearchBar extends Component {

  constructor(props) {
    super(props);
  }

  changeHandler(e) {
    this.props.fetchData(e.target.value);
  }

  render() {
    const { term } = this.props;
    

    return (
      <div className={styles.search}>
      <Icon name="react-logo"/>
      <img className={styles.logo} src={image} alt="" />
      <input type="text" value={term} onChange={ e => this.changeHandler(e)} />
      <div className={styles.text}>جستجو برای "{term}"</div>
      </div >
    );
  }
}

const mapStateToProps = state => ({
  term: state.search.term
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchData
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);