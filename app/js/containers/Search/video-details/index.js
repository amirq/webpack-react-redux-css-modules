import React, { Component } from 'react';
import { connect } from 'react-redux';
import styles from './styles';
import loading from 'scss/loading';

class VideoDetails extends React.Component {

  render() {
    const { search } = this.props;

    if (!search.selected) {
      return (
        <div className={styles.video + ' ' + styles.videoInit}>
         
        </div>
      );
    }
    return (
      <div className={styles.video}>
        <div className={[loading.red, styles.loading].join(" ")}></div>
        <iframe src={search.item.frame}></iframe>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  search: state.search
});

export default connect(mapStateToProps,null)(VideoDetails);