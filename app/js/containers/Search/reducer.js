import { FETCHING_START, FETCHING_DONE, FETCHING_FAILED, SELECT_ITEM } from './constants';

const initialState = {
	term: '',
	fetching: false,
	fetched: false,
	fetchedData: [],
	error: null,
	item: {},
	selected: false
};

export default (state = initialState, { type, payload }) => {

	switch (type) {
		case FETCHING_START:
			state = { ...state, fetching: true, term: payload.term };
			break;
		case FETCHING_DONE:
			state = { ...state, fetching: false, fetched: true, fetchedData: payload.result };
			break;
		case FETCHING_FAILED:
			state = { ...state, fetching: false, error: payload.result };
			break;
		case SELECT_ITEM:
			state = { ...state, item: payload, selected: true };
			break;
	}

	return state;
};