import axios from 'axios';
import { FETCHING_START, FETCHING_DONE, FETCHING_FAILED, SELECT_ITEM } from './constants';

export const fetchData = term => {
	return (dispatch, getState) => new Promise((resolve, reject) => {
		const URL = `/api/videoBySearch/text/${term}/perpage/10`;
		dispatch({ type: FETCHING_START, payload: { term: term } });

		axios(URL)
			.then(res => {
				resolve(true);
				dispatch({ type: FETCHING_DONE, payload: { result: res.data } });
			})
			.catch(error => {
				dispatch({ type: FETCHING_FAILED, payload: { result: error } });
			});
	});
}


export const selectItem = item => ({ type: SELECT_ITEM, payload: item });