import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { selectItem } from '../actions';
import styles from './styles';

class SearchItem extends Component{

  constructor(props){
    super(props);
    this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler(){
    this.props.selectItem(this.props.item);
  }
  render(){
    const item = this.props.item;
    const bg = {backgroundImage: `url(${item.small_poster})`};
    return(
      <li className={styles.item} onClick={this.clickHandler}>
        <div className={styles.itemImg}>
          <img src={item.small_poster} alt={item.title}/>
          <div className={styles.itemInner} style={bg}></div>
        </div>
        <div>
          <p>{item.title}</p>
          <p>ارسال کننده: {item.sender_name}</p>
          <time>{item.sdate}</time>
        </div>
      </li>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
	selectItem
}, dispatch);

export default connect(null,mapDispatchToProps)(SearchItem);