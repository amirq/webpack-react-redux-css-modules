import React, { Component } from 'react';
import SearchItem from '../search-item';
import { connect } from 'react-redux';
import styles from './styles';

class SearchItems extends Component{

  render(){
    const { search } = this.props;

    if(search.term.length < 2  && search.fetched){
      return <div>کلمه کوتاه است!</div>
    }
    if(search.fetching){
      return <div>در حال بارگذاری ...</div>
    }
    if(search.fetchedData.length <=0){
      return <div>لطفا کلمه مورد نظر خود را جستجو کنید</div>;
    }
    if(!search.fetchedData.videobysearch){
      return <div>نتیجه ای یافت نشد</div>;
    }

    return(
      <ul className={styles.list}>
        {search.fetchedData.videobysearch.map(item => <SearchItem key={item.id} item={item}/>) }
      </ul>
    );
  }
}

const mapStateToProps = state => ({
  search: state.search
});

export default connect(mapStateToProps, null)(SearchItems);