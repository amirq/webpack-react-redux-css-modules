import React, { Component, Fragment } from 'react';
import { connect } from 'redux';
import { Route, Switch } from 'react-router-dom';
import { withRouter } from 'react-router';
import { Helmet } from 'react-helmet';
import routesConfig from 'js/routesConfig';

import Home from 'js/components/Home';
import Search from 'js/containers/Search';
//import Header from 'js/containers/Header';

class Layout extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<Fragment>
				<Helmet
					titleTemplate="%s | جستجوی آپارات"
					defaultTitle="جستجوی آپارات"
				>
					<body dir="rtl" />
				</Helmet>
				{/* <Header /> */}
				<Switch>
					<Route exact path={routesConfig.home.path} component={Home}/>
					<Route path={routesConfig.search.path} component={Search}/>
				</Switch>
			</Fragment>
		);
	}
}

export default withRouter(Layout);