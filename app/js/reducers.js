import { combineReducers } from 'redux';

import search from 'js/containers/Search/reducer';

export default combineReducers({
  search
});