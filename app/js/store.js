import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web
import ReduxThunk from 'redux-thunk';
import rootReducer from './reducers';
import Logger from 'redux-logger';

const persistConfig = {
  key: 'root',
	storage,
	whitelist: [ 'search' ], // only these keys will be persisted
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
	persistedReducer,
	composeEnhancers(
	applyMiddleware(ReduxThunk))
);

const persistor = persistStore(store);

export {
	store, persistor
};