import React from 'react';
import ReactDom from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import 'svgxuse';
import { store, persistor } from './store';

import App from 'js/containers/app';


const root = document.getElementById('app');

const render = Component => {
	ReactDom.render(
		<AppContainer>
			<Component store={store} persistor={persistor} />
		</AppContainer>,
		root
	)
};

render(App);

if (module.hot) {
	module.hot.accept('js/containers/app', () => {
		const NextApp = require('js/containers/app').default;
		render(NextApp)
	})
}
