import React, { Component } from 'react';
import { Redirect } from 'react-router';
import routesConfig from 'js/routesConfig';

class Home extends Component{
	constructor(props){
		super(props);
	}

	render() {
		return (
			<Redirect to={routesConfig.search.path}/>
		);
	}
}

export default Home;